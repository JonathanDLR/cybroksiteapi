<?php

namespace Cybrokapi\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Type;

/**
 * Song
 * @ORM\Table(name="song", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="unique_btu", columns={"band", "title", "user_id"})
 * })
 * @ORM\Entity(repositoryClass="Cybrokapi\ApiBundle\Repository\SongRepository")
 * @UniqueEntity(fields={"band", "title", "user"}, message="Ce morceau est déjà enregistré")
 * @ExclusionPolicy("all")
 */
class Song
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="band", type="string", length=255)
     * @Expose
     */
    private $band;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @Expose
     */
    private $title;

    /**
     * @ORM\ManyToOne(targetEntity="Cybrokapi\UserBundle\Entity\User", inversedBy="song", cascade={"persist"}, fetch="EAGER")
     */
    private $user;

    /**
     * @ORM\OneToMany(
     *      targetEntity="Cybrokapi\ApiBundle\Entity\Informations", 
     *      mappedBy="song",
     *      cascade={"persist"}
     * )
     * @Expose
     * @Type("ArrayCollection<Cybrokapi\ApiBundle\Entity\Informations>")
     */
    private $informations;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set band
     *
     * @param string $band
     *
     * @return Song
     */
    public function setBand($band)
    {
        $this->band = $band;

        return $this;
    }

    /**
     * Get band
     *
     * @return string
     */
    public function getBand()
    {
        return $this->band;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Song
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->user = new \Doctrine\Common\Collections\ArrayCollection();
        $this->informations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get user
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user
     *
     * @param \Cybrokapi\UserBundle\Entity\User $user
     *
     * @return Song
     */
    public function setUser(\Cybrokapi\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Add information
     *
     * @param \Cybrokapi\ApiBundle\Entity\Informations $information
     *
     * @return Song
     */
    public function addInformation(\Cybrokapi\ApiBundle\Entity\Informations $information)
    {
        $this->informations[] = $information;

        $information->setSong($this);

        return $this;
    }

    /**
     * Remove information
     *
     * @param \Cybrokapi\ApiBundle\Entity\Informations $information
     */
    public function removeInformation(\Cybrokapi\ApiBundle\Entity\Informations $information)
    {
        $this->informations->removeElement($information);
        $information->removeSong($this);
    }

    /**
     * Get informations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInformations()
    {
        return $this->informations;
    }

    /**
     * Set informations
     *
     * @param \Cybrokapi\ApiBundle\Entity\Informations $informations
     *
     * @return Song
     */
    public function setInformations(\Cybrokapi\ApiBundle\Entity\Informations $informations = null)
    {
        $this->informations = $informations;

        $informations->setSong($this);

        return $this;
    }
}
