<?php

namespace Cybrokapi\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;

/**
 * Informations
 *
 * @ORM\Table(name="informations")
 * @ORM\Entity(repositoryClass="Cybrokapi\ApiBundle\Repository\InformationsRepository")
 * @UniqueEntity(fields={"nameins", "song"}, message="Cet instrument est déjà enregistré")
 * @ExclusionPolicy("all") */
class Informations
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="nameins", type="string", length=100)
     * @Expose
     */
    private $nameins;

    /**
     * @var string
     *
     * @ORM\Column(name="info1", type="string", length=255, nullable=true)
     * @Expose
     */
    private $info1;

    /**
     * @var string
     *
     * @ORM\Column(name="info2", type="string", length=255, nullable=true)
     * @Expose
     */
    private $info2;

    /**
     * @var string
     *
     * @ORM\Column(name="info3", type="string", length=255, nullable=true)
     * @Expose
     */
    private $info3;

    /**
     * @var string
     *
     * @ORM\Column(name="info4", type="string", length=255, nullable=true)
     * @Expose
     */
    private $info4;

    /**
     * @var string
     *
     * @ORM\Column(name="info5", type="string", length=255, nullable=true)
     * @Expose
     */
    private $info5;

    /**
     * @var string
     *
     * @ORM\Column(name="info6", type="string", length=255, nullable=true)
     * @Expose
     */
    private $info6;

    /**
     * @var string
     *
     * @ORM\Column(name="info7", type="string", length=255, nullable=true)
     * @Expose
     */
    private $info7;

    /**
     * @var string
     *
     * @ORM\Column(name="info8", type="string", length=255, nullable=true)
     * @Expose
     */
    private $info8;

    /**
     * @var string
     *
     * @ORM\Column(name="info9", type="string", length=255, nullable=true)
     * @Expose
     */
    private $info9;

    /**
     * @ORM\ManyToOne(targetEntity="Cybrokapi\ApiBundle\Entity\Song", inversedBy="informations", fetch="EAGER")
     * @ORM\JoinColumn(name="song_id", referencedColumnName="id", nullable=false)
     */
    private $song;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set info1
     *
     * @param string $info1
     *
     * @return Informations
     */
    public function setInfo1($info1)
    {
        $this->info1 = $info1;

        return $this;
    }

    /**
     * Get info1
     *
     * @return string
     */
    public function getInfo1()
    {
        return $this->info1;
    }

    /**
     * Set info2
     *
     * @param string $info2
     *
     * @return Informations
     */
    public function setInfo2($info2)
    {
        $this->info2 = $info2;

        return $this;
    }

    /**
     * Get info2
     *
     * @return string
     */
    public function getInfo2()
    {
        return $this->info2;
    }

    /**
     * Set info3
     *
     * @param string $info3
     *
     * @return Informations
     */
    public function setInfo3($info3)
    {
        $this->info3 = $info3;

        return $this;
    }

    /**
     * Get info3
     *
     * @return string
     */
    public function getInfo3()
    {
        return $this->info3;
    }

    /**
     * Set info4
     *
     * @param string $info4
     *
     * @return Informations
     */
    public function setInfo4($info4)
    {
        $this->info4 = $info4;

        return $this;
    }

    /**
     * Get info4
     *
     * @return string
     */
    public function getInfo4()
    {
        return $this->info4;
    }

    /**
     * Set info5
     *
     * @param string $info5
     *
     * @return Informations
     */
    public function setInfo5($info5)
    {
        $this->info5 = $info5;

        return $this;
    }

    /**
     * Get info5
     *
     * @return string
     */
    public function getInfo5()
    {
        return $this->info5;
    }

    /**
     * Set info6
     *
     * @param string $info6
     *
     * @return Informations
     */
    public function setInfo6($info6)
    {
        $this->info6 = $info6;

        return $this;
    }

    /**
     * Get info6
     *
     * @return string
     */
    public function getInfo6()
    {
        return $this->info6;
    }

    /**
     * Set info7
     *
     * @param string $info7
     *
     * @return Informations
     */
    public function setInfo7($info7)
    {
        $this->info7 = $info7;

        return $this;
    }

    /**
     * Get info7
     *
     * @return string
     */
    public function getInfo7()
    {
        return $this->info7;
    }

    /**
     * Set info8
     *
     * @param string $info8
     *
     * @return Informations
     */
    public function setInfo8($info8)
    {
        $this->info8 = $info8;

        return $this;
    }

    /**
     * Get info8
     *
     * @return string
     */
    public function getInfo8()
    {
        return $this->info8;
    }

    /**
     * Set info9
     *
     * @param string $info9
     *
     * @return Informations
     */
    public function setInfo9($info9)
    {
        $this->info9 = $info9;

        return $this;
    }

    /**
     * Get info9
     *
     * @return string
     */
    public function getInfo9()
    {
        return $this->info9;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Informations
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set nameins
     *
     * @param string $nameins
     *
     * @return Informations
     */
    public function setNameins($nameins)
    {
        $this->nameins = $nameins;

        return $this;
    }

    /**
     * Get nameins
     *
     * @return string
     */
    public function getNameins()
    {
        return $this->nameins;
    }

    /**
     * Set song
     *
     * @param \Cybrokapi\ApiBundle\Entity\Song $song
     *
     * @return Informations
     */
    public function setSong(\Cybrokapi\ApiBundle\Entity\Song $song)
    {
        $this->song = $song;

        return $this;
    }

    /**
     * Get song
     *
     * @return \Cybrokapi\ApiBundle\Entity\Song
     */
    public function getSong()
    {
        return $this->song;
    }
}
