<?php

namespace Cybrokapi\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Request\ParamFetcher;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Validator\ConstraintViolationList;
use AppBundle\Exception\ResourceValidationException;
use Cybrokapi\ApiBundle\Entity\Song;

class DefaultController extends FOSRestController
{
    /**
     * @Rest\Post("/songs/create", name="song_create")
     * @Rest\View(StatusCode = 201)
     * @Rest\QueryParam(name="apikey")
     * @ParamConverter("song", converter="fos_rest.request_body")
     */
    public function createAction(Song $song, ParamFetcher $paramFetcher)
    {
        $apikey = $paramFetcher->get('apikey');

        $user = $this->getDoctrine()->getRepository('CybrokapiUserBundle:User')->findOneByApikey($apikey);

        if (empty($user)) {
            throw new \Exception("Votre clef n'est pas valide!");
        }

        $song->setUser($user);

        foreach ($song->getInformations() as $information) {
            $information->setSong($song);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($song);
        $em->flush();

        return $song;
    }

    /**
     * @Rest\Get("/songs", name="song_show")
     * @Rest\View()
     * @Rest\QueryParam(name="band")
     * @Rest\QueryParam(name="title")
     * @Rest\QueryParam(name="apikey")
     */
    public function showAction(Song $song, ParamFetcher $paramFetcher)
    {
        $apikey = $paramFetcher->get('apikey');

        $user = $this->getDoctrine()->getRepository('CybrokapiUserBundle:User')->findOneByApikey($apikey);

        if (empty($user)) {
            throw new \Exception("Votre clef n'est pas valide!");
        }

        $song = $this->getDoctrine()->getRepository('CybrokapiApiBundle:Song')->search(
            $paramFetcher->get('band'),
            $paramFetcher->get('title'),
            $user
        );

        if (empty($song)) {
            throw new \Exception("Ce morceau n'existe pas");
        }

        return $song;
    }

     /**
     * @Rest\Get("/songs/info", name="info_show")
     * @Rest\View()
     * @Rest\QueryParam(name="band")
     * @Rest\QueryParam(name="title")
     * @Rest\QueryParam(name="apikey")
     */
    public function infoAction(Song $song, ParamFetcher $paramFetcher)
    {
        $apikey = $paramFetcher->get('apikey');

        $user = $this->getDoctrine()->getRepository('CybrokapiUserBundle:User')->findOneByApikey($apikey);

        if (empty($user)) {
            throw new \Exception("Votre clef n'est pas valide!");
        }

        $song = $this->getDoctrine()->getRepository('CybrokapiApiBundle:Song')->searchInfo(
            $paramFetcher->get('band'),
            $paramFetcher->get('title'),
            $user
        );

        if (empty($song)) {
            throw new \Exception("Ce morceau n'existe pas");
        }

        return $song;
    }

    /**
     * @Rest\Put("/songs/update", name="song_update")
     * @Rest\View(serializerGroups={"instrument", "info"})
     * @Rest\QueryParam(name="band")
     * @Rest\QueryParam(name="title")
     * @Rest\QueryParam(name="apikey")  
     * @ParamConverter("songUpdated", converter="fos_rest.request_body")
     */
    public function updateAction(Song $songUpdated, ParamFetcher $paramFetcher)
    {
        $em = $this->getDoctrine()->getManager();

        $apikey = $paramFetcher->get('apikey');

        $user = $this->getDoctrine()->getRepository('CybrokapiUserBundle:User')->findOneByApikey($apikey);

        if (empty($user)) {
            throw new \Exception("Votre clef n'est pas valide!");
        }

        $song = $this->getDoctrine()->getRepository('CybrokapiApiBundle:Song')->search(
            $paramFetcher->get('band'),
            $paramFetcher->get('title'),
            $user
        );

        if (empty($song)) {
            throw new \Exception("Ce morceau n'existe pas");
        }

        $song->setBand($songUpdated->getBand());
        $song->setTitle($songUpdated->getTitle());

        foreach($song->getInformations() as $information) {
            $song->removeInformation($information);
            $em->remove($information);
        }

        foreach ($songUpdated->getInformations() as $information) {
            $song->addInformation($information);
            $em->persist($information);
        }
       
        $em->persist($song);
        $em->flush();

        return $song;
    }

    /**
     * @Rest\Delete("/songs/delete", name="song_delete")
     * @Rest\QueryParam(name="band")
     * @Rest\QueryParam(name="title")
     * @Rest\QueryParam(name="apikey")
     */
    public function deleteAction(Song $song, ParamFetcher $paramFetcher)
    {
        $apikey = $paramFetcher->get('apikey');

        $user = $this->getDoctrine()->getRepository('CybrokapiUserBundle:User')->findOneByApikey($apikey);

        if (empty($user)) {
            throw new \Exception("Votre clef n'est pas valide!");
        }

        $song = $this->getDoctrine()->getRepository('CybrokapiApiBundle:Song')->search(
            $paramFetcher->get('band'),
            $paramFetcher->get('title'),
            $user
        );

        if (empty($song)) {
            throw new \Exception("Ce morceau n'existe pas");
        }

        $em = $this->getDoctrine()->getManager();
        foreach ($song->getInformations() as $information) {
            $song->removeInformation($information);
            $em->remove($information);
        }
        $em->remove($song);
        $em->flush();

        $response = "Song removed";

        return $response;
    }
}
