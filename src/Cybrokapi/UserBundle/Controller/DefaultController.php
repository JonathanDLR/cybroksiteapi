<?php

namespace Cybrokapi\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Cybrokapi\UserBundle\Entity\User;
use Cybrokapi\UserBundle\Form\UserType;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    public function registerAction(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $user = new User();
        $form = $this->get('form.factory')->create(UserType::class, $user);

        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $constraint = $constraint = new \Symfony\Component\Validator\Constraints\Email();
            $error = $this->get('validator')->validate($user->getEmail(), $constraint);

            if(count($error) > 0) {
                return $this->render('@CybrokapiUser/Register/errormail.html.twig');
            } else {
                $password = $passwordEncoder->encodePassword($user, $user->getPassword());
                $user->setPassword($password);
    
                $apikey = $user->apikeyGeneration();
                $user->setApikey($apikey);
    
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();
    
                return $this->render('@CybrokapiUser/Register/confirm.html.twig', array(
                    'key' => $user->getApikey())
                );
            }
        }

        return $this->render('@CybrokapiUser/Register/index.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function retrieveAction(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $user = new User();
        $form = $this->get('form.factory')->create(UserType::class, $user);

        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $userFind = $this->getDoctrine()->getRepository('CybrokapiUserBundle:User')->findOneByEmail($user->getEmail());

            if ($userFind) {
                $encoderService = $this->container->get('security.password_encoder');
                $match = $encoderService->isPasswordValid($userFind, $user->getPassword());
    
                if ($match) {
                    return $this->render('@CybrokapiUser/Register/confirm.html.twig', array(
                        'key' => $userFind->getApikey()
                    ));
                } else {
                    return $this->render('@CybrokapiUser/Register/errorapi.html.twig');
                }
            } else {
                return $this->render('@CybrokapiUser/Register/errorapi.html.twig');
            }
        }

        return $this->render('@CybrokapiUser/Register/retrieve.html.twig', array(
            'form' => $form->createView()
        ));
    }
}
