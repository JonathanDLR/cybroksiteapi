<?php

namespace Cybrokapi\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="Cybrokapi\UserBundle\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="apikey", type="string", length=255, unique=true)
     */
    private $apikey;

    // private $roles;

    // private $salt;

    // private $userName;

    /**
     * @ORM\OneToMany(targetEntity="Cybrokapi\ApiBundle\Entity\Song", mappedBy="user")
     * @ORM\JoinColumn(nullable=false)
     */
    private $song;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    public function apikeyGeneration()
    {
        $apikey = openssl_random_pseudo_bytes(64);
        $apikey = bin2hex($apikey);

        return $apikey;
    }

    /**
     * Set apikey
     *
     * @param string $apikey
     *
     * @return User
     */
    public function setApikey($apikey)
    {
        $this->apikey = $apikey;

        return $this;
    }

    /**
     * Get apikey
     *
     * @return string
     */
    public function getApikey()
    {
        return $this->apikey;
    }

    public function getRoles()
    {

    }

    public function getSalt()
    {

    }

    public function getUserName()
    {

    }

    public function eraseCredentials()
    {

    }

    /**
     * Set song
     *
     * @param \Cybrokapi\ApiBundle\Entity\Song $song
     *
     * @return User
     */
    public function setSong(\Cybrokapi\ApiBundle\Entity\Song $song)
    {
        $this->song = $song;

        return $this;
    }

    /**
     * Get song
     *
     * @return \Cybrokapi\ApiBundle\Entity\Song
     */
    public function getSong()
    {
        return $this->song;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->song = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add song
     *
     * @param \Cybrokapi\ApiBundle\Entity\Song $song
     *
     * @return User
     */
    public function addSong(\Cybrokapi\ApiBundle\Entity\Song $song)
    {
        $this->song[] = $song;

        return $this;
    }

    /**
     * Remove song
     *
     * @param \Cybrokapi\ApiBundle\Entity\Song $song
     */
    public function removeSong(\Cybrokapi\ApiBundle\Entity\Song $song)
    {
        $this->song->removeElement($song);
    }
}
