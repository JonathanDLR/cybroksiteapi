COPY = {
    init: function() {
        document.getElementById("copy").addEventListener('click', COPY.copy);
    },

    copy: function() {
        var range = document.createRange();
        range.selectNode(document.getElementById("key"));
        window.getSelection().removeAllRanges();
        window.getSelection().addRange(range);
        document.execCommand("copy")
    }
}

window.onload = COPY.init();